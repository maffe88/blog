class Comment < ActiveRecord::Base
  belongs_to :comments
  validates_presence_of :post_id
  validates_presence_of :body
end
